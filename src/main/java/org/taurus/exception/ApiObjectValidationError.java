package org.taurus.exception;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Set;

@ApiModel(description = "Used to logically group multiple ApiFieldValidationError")
public class ApiObjectValidationError {

	@ApiModelProperty(readOnly = true)
	private final Set<ApiFieldValidationError> fieldErrors;

	@JsonCreator
	public ApiObjectValidationError(@JsonProperty("fieldErrors") Set<ApiFieldValidationError> fieldErrors) {
		this.fieldErrors = fieldErrors;
	}

	public Set<ApiFieldValidationError> getFieldErrors() {
		return fieldErrors;
	}
}
