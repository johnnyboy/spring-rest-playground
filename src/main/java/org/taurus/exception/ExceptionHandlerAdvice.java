package org.taurus.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@ControllerAdvice
public class ExceptionHandlerAdvice {

	@ExceptionHandler(ApiException.class)
	public ResponseEntity<ApiError> handleApiException(ApiException e) {
		return new ResponseEntity<>(new ApiError(e.getStatusCode().getReasonPhrase(), e.getMessage()), e.getStatusCode());
	}

	@ExceptionHandler(ApiObjectValidationException.class)
	public ResponseEntity<ApiObjectValidationError> handleApiObjectValidationException(ApiObjectValidationException e) {
		Errors errors = e.getErrors();
		Set<ApiFieldValidationError> fieldErrors = errors.getFieldErrors().stream()
				.map(err -> new ApiFieldValidationError(err.getField(), err.getRejectedValue(), err.getDefaultMessage()))
				.collect(Collectors.toSet());
		return new ResponseEntity<>(new ApiObjectValidationError(fieldErrors), e.getStatusCode());
	}
}
