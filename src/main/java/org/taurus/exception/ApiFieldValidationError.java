package org.taurus.exception;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

@ApiModel(description = "Used to communicate human-readable field validation error")
public class ApiFieldValidationError {

	@ApiModelProperty(readOnly = true, example = "email")
	private final String field;

	@ApiModelProperty(readOnly = true, example = "bob.hotmail.com", position = 1)
	private final Object value;

	@ApiModelProperty(readOnly = true, example = "must be a well-formed email address", position = 2)
	private final String message;

	@JsonCreator
	public ApiFieldValidationError(@JsonProperty("field") String field,
								   @JsonProperty("value") Object value,
								   @JsonProperty("message") String message) {
		this.field = field;
		this.value = value;
		this.message = message;
	}

	public String getField() {
		return field;
	}

	public Object getValue() {
		return value;
	}

	public String getMessage() {
		return message;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		ApiFieldValidationError that = (ApiFieldValidationError) o;
		return Objects.equals(getField(), that.getField()) &&
				Objects.equals(String.valueOf(getValue()), String.valueOf(that.getValue())) &&
				Objects.equals(getMessage(), that.getMessage());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getField(), getValue(), getMessage());
	}

	@Override
	public String toString() {
		return String.format("ApiFieldValidationError{field:%s, value:%s, message:%s}", getField(), getValue(), getMessage());
	}
}
