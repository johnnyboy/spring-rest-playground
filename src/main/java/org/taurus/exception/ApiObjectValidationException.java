package org.taurus.exception;

import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;

public class ApiObjectValidationException extends ApiException {
	private final Errors errors;

	public ApiObjectValidationException(Errors errors) {
		super(HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.getReasonPhrase());
		this.errors = errors;
	}

	public Errors getErrors() {
		return errors;
	}
}
