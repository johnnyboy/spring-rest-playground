package org.taurus.exception;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Generic ApiError for communicating exceptional cases")
public class ApiError {

	@ApiModelProperty(notes = "HTTPS status code reason phrase", readOnly = true, example = "Not Found")
	private final String statusCode;

	@ApiModelProperty(notes = "Human-readable error details", readOnly = true, example = "No Author found for id 5387")
	private final String message;

	@JsonCreator
	public ApiError(@JsonProperty("statusCode") String statusCode, @JsonProperty("message") String message) {
		this.statusCode = statusCode;
		this.message = message;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public String getMessage() {
		return message;
	}
}

