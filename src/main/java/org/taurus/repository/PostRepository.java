package org.taurus.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.taurus.domain.Author;
import org.taurus.domain.Post;
import org.taurus.domain.dto.PostSummary;

public interface PostRepository extends CrudRepository<Post, Long>, JpaSpecificationExecutor<Post> {

	@Query("SELECT new org.taurus.domain.dto.PostSummary(p.id, p.title, p.content, p.timestamp, a.id) FROM Post p JOIN p.author a WHERE a = :author ORDER BY p.timestamp DESC")
	Page<PostSummary> findAllSummariesByAuthor(@Param("author") Author author, Pageable pageable);

	@Query("SELECT new org.taurus.domain.dto.PostSummary(p.id, p.title, p.content, p.timestamp, a.id) FROM Post p JOIN p.author a WHERE p.id = :id")
	PostSummary findSummary(@Param("id") Long id);
}
