package org.taurus.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.taurus.domain.Author;
import org.taurus.domain.dto.AuthorSummary;

public interface AuthorRepository extends CrudRepository<Author, Long> {

	@Query("SELECT new org.taurus.domain.dto.AuthorSummary(a.id, a.firstName, a.lastName, a.email, COUNT(p)) FROM Author a LEFT JOIN a.posts p GROUP BY a.id ORDER BY a.lastName ASC")
	Page<AuthorSummary> findAllSummaries(Pageable pageable);

	@Query("SELECT new org.taurus.domain.dto.AuthorSummary(a.id, a.firstName, a.lastName, a.email, COUNT(p)) FROM Author a LEFT JOIN a.posts p WHERE a.id = :id")
	AuthorSummary findSummary(@Param("id") Long id);

	@Query("SELECT a FROM Author a LEFT JOIN FETCH a.posts WHERE a.id = :id")
	Author findAuthorAndPosts(@Param("id")Long id);
}
