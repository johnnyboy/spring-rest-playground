package org.taurus.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.taurus.domain.Post;
import org.taurus.domain.dto.PostSummary;
import org.taurus.domain.dto.paging.PagedResponse;
import org.taurus.repository.PostRepository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PostSearchService {

	private final PostRepository repository;

	public PostSearchService(PostRepository repository) {
		this.repository = repository;
	}

	public PagedResponse<PostSummary> findBy(String title, String content, Long author, int page, int pageSize) {
		PageRequest pageRequest = PageRequest.of(page, pageSize, Sort.by(Sort.Direction.DESC, "timestamp"));
		Specification<Post> spec = (root, query, cb) -> predicate(title, content, author, root, cb);
		Page<Post> pageOfPosts = repository.findAll(spec, pageRequest);
		List<PostSummary> postSummaries = pageOfPosts.getContent().stream().map(this::asPostSummary).collect(Collectors.toList());
		return pagedResponse(pageOfPosts, postSummaries);
	}

	private PagedResponse<PostSummary> pagedResponse(Page<Post> pageOfPosts, List<PostSummary> postSummaries) {
		return new PagedResponse<>(
				pageOfPosts.hasContent(),
				pageOfPosts.hasNext(),
				pageOfPosts.hasPrevious(),
				pageOfPosts.getTotalPages(),
				pageOfPosts.getTotalElements(),
				pageOfPosts.getSize(),
				pageOfPosts.getNumber(),
				pageOfPosts.getNumberOfElements(),
				postSummaries);
	}

	private PostSummary asPostSummary(Post p) {
		return new PostSummary(p.getId(), p.getTitle(), p.getContent(), p.getTimestamp(), p.getAuthor().getId());
	}

	private Predicate predicate(String title, String content, Long author, Root<Post> root, CriteriaBuilder cb) {
		List<Predicate> predicates = new ArrayList<>();
		if (author != null && author > 0) {
			predicates.add(cb.equal(root.get("author"), author));
		}
		if (!StringUtils.isEmpty(title)) {
			predicates.add(cb.like(cb.lower(root.get("title")), like(title)));
		}
		if (!StringUtils.isEmpty(content)) {
			predicates.add(cb.like(cb.lower(root.get("content")), like(content)));
		}
		return cb.and(predicates.toArray(new Predicate[predicates.size()]));
	}

	private String like(String value) {
		return "%".concat(value.toLowerCase()).concat("%");
	}

}
