package org.taurus.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.taurus.domain.Author;
import org.taurus.domain.dto.AuthorPayload;
import org.taurus.domain.dto.AuthorSummary;
import org.taurus.domain.dto.paging.PagedResponse;
import org.taurus.exception.ApiException;
import org.taurus.repository.AuthorRepository;

@Service
@Transactional(readOnly = true)
public class AuthorService {

	private final AuthorRepository repository;

	@Autowired
	public AuthorService(AuthorRepository repository) {
		this.repository = repository;
	}

	@Transactional
	public Author save(String firstName, String lastName, String email) {
		return repository.save(new Author(firstName, lastName, email));
	}

	public Author find(Long id) {
		return repository.findById(id).orElseThrow(() -> new ApiException(HttpStatus.NOT_FOUND, String.format("No Author found for id %d", id)));
	}

	public Author findWithPosts(Long authorId){
		return repository.findAuthorAndPosts(authorId);
	}

	public boolean exists(Long id) {
		return repository.existsById(id);
	}

	@Transactional
	public Author update(Author author, AuthorPayload payload) {
		author.setEmail(payload.getEmail());
		author.setFirstName(payload.getFirstName());
		author.setLastName(payload.getLastName());
		return repository.save(author);
	}

	@Transactional
	public void delete(Long id) {
		repository.deleteById(id);
	}

	public AuthorSummary findSummary(Long id) {
		return repository.findSummary(id);
	}

	public PagedResponse<AuthorSummary> findAllSummaries(int page, int pageSize) {
		Page<AuthorSummary> paged = repository.findAllSummaries(PageRequest.of(page, pageSize));
		return new PagedResponse<>(paged);
	}
}
