package org.taurus.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.taurus.domain.Author;
import org.taurus.domain.Post;
import org.taurus.domain.dto.PostPayload;
import org.taurus.domain.dto.PostSummary;
import org.taurus.domain.dto.paging.PagedResponse;
import org.taurus.exception.ApiException;
import org.taurus.repository.PostRepository;

import java.time.LocalDateTime;

@Service
@Transactional(readOnly = true)
public class PostService {

	private final PostRepository repository;

	@Autowired
	public PostService(PostRepository repository) {
		this.repository = repository;
	}

	@Transactional
	public Post add(Author author, PostPayload payload) {
		Post post = repository.save(new Post(payload.getTitle(), payload.getContent()));
		author.addPost(post);
		return post;
	}

	public Post find(Long id) {
		return repository.findById(id).orElseThrow(() -> new ApiException(HttpStatus.NOT_FOUND, String.format("No Post found for id %d", id)));
	}

	public PostSummary findSummary(Long id) {
		return repository.findSummary(id);
	}

	@Transactional
	public Post add(Author author, String title, String content) {
		Post post = repository.save(new Post(title, content));
		author.addPost(post);
		return post;
	}

	public boolean exists(Long id) {
		return repository.existsById(id);
	}

	@Transactional
	public void delete(Author author, Post post) {
		author.removePost(post);
		repository.delete(post);
	}

	public PagedResponse<PostSummary> findByAuthor(Author author, int page, int pageSize) {
		Page<PostSummary> paged = repository.findAllSummariesByAuthor(author, PageRequest.of(page, pageSize));
		return new PagedResponse<>(paged);
	}

	public void update(Post post, PostPayload payload) {
		post.setTitle(payload.getTitle());
		post.setContent(payload.getContent());
		post.setTimestamp(LocalDateTime.now());
	}
}
