package org.taurus.config;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.ShallowEtagHeaderFilter;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;

@Configuration
public class RestConfiguration {
	private final int timeout = (int) TimeUnit.SECONDS.toMillis(5);

	@Bean
	public FilterRegistrationBean<ShallowEtagHeaderFilter> shallowEtagHeaderFilter() {
		FilterRegistrationBean<ShallowEtagHeaderFilter> filterRegistrationBean = new FilterRegistrationBean<>();
		filterRegistrationBean.setFilter(new ShallowEtagHeaderFilter());
		filterRegistrationBean.addUrlPatterns("/api/*");
		return filterRegistrationBean;
	}

	@Bean
	public ObjectMapper objectMapper() {
		ObjectMapper om = new ObjectMapper();
		om.disable(SerializationFeature.INDENT_OUTPUT);
		om.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
		om.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
		om.setDateFormat(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss"));
		om.registerModule(new JavaTimeModule());
		return om;
	}

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder, ObjectMapper objectMapper) {
		return builder
				.setConnectTimeout(timeout)
				.setReadTimeout(timeout)
				.messageConverters(new MappingJackson2HttpMessageConverter(objectMapper))
				.interceptors(new AcceptJsonUtf8Interceptor())
				.errorHandler(new ApiResponseErrorHandler())
				.build();
	}

	private class AcceptJsonUtf8Interceptor implements ClientHttpRequestInterceptor {

		@Override
		public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
			request.getHeaders().add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_UTF8_VALUE);
			return execution.execute(request, body);
		}
	}

	private class ApiResponseErrorHandler extends DefaultResponseErrorHandler {
		@Override
		protected boolean hasError(HttpStatus statusCode) {
			return statusCode.is5xxServerError();
		}
	}
}
