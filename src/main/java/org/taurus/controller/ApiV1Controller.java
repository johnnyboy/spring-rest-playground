package org.taurus.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;
import org.taurus.domain.Author;
import org.taurus.domain.Post;
import org.taurus.domain.dto.AuthorPayload;
import org.taurus.domain.dto.AuthorSummary;
import org.taurus.domain.dto.PostPayload;
import org.taurus.domain.dto.PostSummary;
import org.taurus.domain.dto.paging.PagedResponse;
import org.taurus.exception.ApiError;
import org.taurus.exception.ApiException;
import org.taurus.exception.ApiObjectValidationError;
import org.taurus.exception.ApiObjectValidationException;
import org.taurus.service.AuthorService;
import org.taurus.service.PostSearchService;
import org.taurus.service.PostService;

import javax.validation.Valid;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@Api(description = "CRUD operations on Authors and Posts")
@RestController
@RequestMapping(path = "/api/v1", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class ApiV1Controller {

	private final AuthorService authorService;
	private final PostService postService;
	private final PostSearchService postSearchService;

	@Autowired
	public ApiV1Controller(AuthorService authorService, PostService postService, PostSearchService postSearchService) {
		this.authorService = authorService;
		this.postService = postService;
		this.postSearchService = postSearchService;
	}

	@ApiOperation(value = "List all AuthorSummaries")
	@GetMapping(path = "/authors")
	public PagedResponse<AuthorSummary> findAllAuthors(@RequestParam(name = "page", required = false, defaultValue = "0") int page,
													   @RequestParam(name = "pageSize", required = false, defaultValue = "10") int pageSize) {
		return authorService.findAllSummaries(page, pageSize);
	}

	@ApiOperation(value = "Find AuthorSummary by id")
	@ApiResponses({
			@ApiResponse(code = 200, message = "returns matching AuthorSummary"),
			@ApiResponse(code = 404, message = "returns ApiError if no author was found", response = ApiError.class)}
	)
	@GetMapping(path = "/authors/{id}")
	public AuthorSummary findAuthor(@PathVariable("id") Long id) {
		assertAuthorExists(id);
		return authorService.findSummary(id);
	}

	@ApiOperation(value = "Create Author by AuthorPayload", code = 201)
	@ApiResponses({
			@ApiResponse(code = 201, message = "creates Author and returns it's URI in the Location header", responseHeaders = @ResponseHeader(name = "Location", description = "Created Author's URI", response = URI.class)),
			@ApiResponse(code = 400, message = "returns ApiObjectValidationError on malformed AuthorPayload", response = ApiObjectValidationError.class)}
	)
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping(path = "/authors", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> createAuthor(@RequestBody @Valid AuthorPayload payload, Errors errors) {
		assertValidation(errors);
		Author author = authorService.save(payload.getFirstName(), payload.getLastName(), payload.getEmail());
		URI authorUri = ServletUriComponentsBuilder.fromCurrentRequestUri().pathSegment(String.valueOf(author.getId())).build().toUri();
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(authorUri);
		return new ResponseEntity<>(headers, HttpStatus.CREATED);
	}

	private void assertValidation(Errors errors) {
		if (errors.hasErrors()) {
			throw new ApiObjectValidationException(errors);
		}
	}

	@ApiOperation(value = "Update given Author by AuthorPayload", code = 204)
	@ApiResponses({
			@ApiResponse(code = 204, message = "updates Author"),
			@ApiResponse(code = 400, message = "returns ApiObjectValidationError on malformed AuthorPayload", response = ApiObjectValidationError.class),
			@ApiResponse(code = 404, message = "returns ApiError if no Author was found", response = ApiError.class)}
	)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@PutMapping(path = "/authors/{id}")
	public void updateAuthor(@RequestBody @Valid AuthorPayload payload, Errors errors, @PathVariable("id") Long id) {
		assertValidation(errors);
		assertAuthorExists(id);
		Author author = authorService.find(id);
		authorService.update(author, payload);
	}

	@ApiOperation(value = "Delete Author by id", code = 204)
	@ApiResponses({
			@ApiResponse(code = 204, message = "deletes Author"),
			@ApiResponse(code = 404, message = "returns ApiError if no Author was found", response = ApiError.class)}
	)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@DeleteMapping(path = "/authors/{id}")
	public void deleteAuthor(@PathVariable("id") Long id) {
		assertAuthorExists(id);
		authorService.delete(id);
	}

	@ApiOperation(value = "List all PostSummaries by given Author")
	@ApiResponses({
			@ApiResponse(code = 200, message = "returns PagedResponse of author posts"),
			@ApiResponse(code = 404, message = "returns ApiError if no Author was found", response = ApiError.class)}
	)
	@GetMapping(path = "/authors/{authorId}/posts")
	public PagedResponse<PostSummary> authorPosts(@PathVariable("authorId") Long id,
												  @RequestParam(name = "page", required = false, defaultValue = "0") int page,
												  @RequestParam(name = "pageSize", required = false, defaultValue = "10") int pageSize) {
		assertAuthorExists(id);
		Author author = authorService.find(id);
		return postService.findByAuthor(author, page, pageSize);
	}

	@ApiOperation(value = "Find PostSummary by id and Author")
	@ApiResponses({
			@ApiResponse(code = 200, message = "returns matching PostSummary"),
			@ApiResponse(code = 404, message = "returns ApiError if no Author was found", response = ApiError.class),
			@ApiResponse(code = 404, message = "returns ApiError if no Post was found", response = ApiError.class)}
	)
	@GetMapping(path = "/authors/{authorId}/posts/{postId}")
	public PostSummary authorPost(@PathVariable("authorId") Long authorId, @PathVariable("postId") Long postId) {
		assertAuthorExists(authorId);
		assertPostExists(postId);
		return postService.findSummary(postId);
	}

	@ApiOperation(value = "Update given Author's Post by PostPayload", code = 204)
	@ApiResponses({
			@ApiResponse(code = 204, message = "updates given Post"),
			@ApiResponse(code = 400, message = "returns ApiObjectValidationError on malformed payload", response = ApiObjectValidationError.class),
			@ApiResponse(code = 404, message = "returns ApiError if no Author was found", response = ApiError.class),
			@ApiResponse(code = 404, message = "returns ApiError if no Post was found", response = ApiError.class)}
	)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@PutMapping(path = "/authors/{authorId}/posts/{postId}", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public void updatePost(@RequestBody @Valid PostPayload payload, Errors errors, @PathVariable("authorId") Long authorId, @PathVariable("postId") Long postId) {
		assertValidation(errors);
		assertAuthorExists(authorId);
		assertPostExists(postId);
		Post post = postService.find(postId);
		postService.update(post, payload);
	}

	@ApiOperation(value = "Delete Post by id", code = 204)
	@ApiResponses({
			@ApiResponse(code = 204, message = "deletes a given Post"),
			@ApiResponse(code = 404, message = "returns ApiError if no Author was found", response = ApiError.class),
			@ApiResponse(code = 404, message = "returns ApiError if no Post was found", response = ApiError.class)}
	)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@DeleteMapping(path = "/authors/{authorId}/posts/{postId}")
	public void deletePost(@PathVariable("authorId") Long authorId, @PathVariable("postId") Long postId) {
		assertAuthorExists(authorId);
		assertPostExists(postId);
		Author author = authorService.findWithPosts(authorId);
		Post post = postService.find(postId);
		postService.delete(author, post);
	}

	@ApiOperation(value = "Create Post by PostPayload for given Author", code = 201)
	@ApiResponses({
			@ApiResponse(code = 201, message = "creates a Post, returns it's URI in Location header", responseHeaders = @ResponseHeader(name = "Location", description = "Created Post's URI", response = URI.class)),
			@ApiResponse(code = 400, message = "returns ApiObjectValidationError on malformed payload", response = ApiObjectValidationError.class),
			@ApiResponse(code = 404, message = "returns ApiError if no Author was found", response = ApiError.class)}
	)
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping(path = "/authors/{id}/posts", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> addPost(@RequestBody @Valid PostPayload payload, Errors errors, @PathVariable("id") Long id) {
		assertValidation(errors);
		assertAuthorExists(id);
		Author author = authorService.findWithPosts(id);
		Post post = postService.add(author, payload);
		Map<String, Object> uriVariables = new HashMap<>();
		uriVariables.put("authorId", id);
		uriVariables.put("postId", post.getId());
		URI postUri = UriComponentsBuilder.fromPath("/api/v1/authors/{authorId}/posts/{postId}").buildAndExpand(uriVariables).toUri();
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(postUri);
		return new ResponseEntity<>(headers, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Search across all PostSummaries")
	@GetMapping(path = "/search")
	public PagedResponse<PostSummary> searchPosts(@RequestParam(name = "title", required = false) String title,
												  @RequestParam(name = "content", required = false) String content,
												  @RequestParam(name = "author", required = false) Long author,
												  @RequestParam(name = "page", required = false, defaultValue = "0") int page,
												  @RequestParam(name = "pageSize", required = false, defaultValue = "10") int pageSize) {
		return postSearchService.findBy(title, content, author, page, pageSize);
	}

	private void assertAuthorExists(Long id) {
		if (!authorService.exists(id)) {
			throw new ApiException(HttpStatus.NOT_FOUND, String.format("No Author found for id %d", id));
		}
	}

	private void assertPostExists(Long id) {
		if (!postService.exists(id)) {
			throw new ApiException(HttpStatus.NOT_FOUND, String.format("No Post found for id %d", id));
		}
	}

}
