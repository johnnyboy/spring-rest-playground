package org.taurus.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
public class Post {

	@Id
	@GeneratedValue
	private Long id;

	private String title;

	private String content;

	private LocalDateTime timestamp;

	@ManyToOne
	private Author author;

	public Post(String title, String content) {
		this.title = title;
		this.content = content;
		this.timestamp = LocalDateTime.now();
	}

	protected Post() {
	}

	public Long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Post post = (Post) o;
		return Objects.equals(getId(), post.getId());
	}

	@Override
	public int hashCode() {
		return 31;
	}
}
