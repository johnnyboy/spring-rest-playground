package org.taurus.domain.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.taurus.domain.dto.paging.PagedContent;

@ApiModel(description = "Read-only author view")
public class AuthorSummary implements PagedContent {

	@ApiModelProperty(readOnly = true, example = "3")
	private final Long id;

	@ApiModelProperty(readOnly = true, example = "Bob", position = 1)
	private final String firstName;

	@ApiModelProperty(readOnly = true, example = "Bobson", position = 2)
	private final String lastName;

	@ApiModelProperty(readOnly = true, example = "bobson@hotmail.com", position = 3)
	private final String email;

	@ApiModelProperty(readOnly = true, example = "5", position = 4)
	private final long postsCount;

	@JsonCreator
	public AuthorSummary(@JsonProperty("id") Long id,
						 @JsonProperty("firstName") String firstName,
						 @JsonProperty("lastName") String lastName,
						 @JsonProperty("email") String email,
						 @JsonProperty("postsCount") long postsCount) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.postsCount = postsCount;
	}

	public Long getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getEmail() {
		return email;
	}

	public long getPostsCount() {
		return postsCount;
	}
}
