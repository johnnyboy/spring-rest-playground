package org.taurus.domain.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@ApiModel(description = "Used to create and update posts")
public class PostPayload {

	@NotNull
	@Positive
	@ApiModelProperty(required = true, allowableValues = "range[1,infinity]", example = "3")
	private Long author;

	@NotBlank
	@Size(max = 30)
	@ApiModelProperty(required = true, allowableValues = "range[1,30]", example = "Procrastination 101", position = 1)
	private String title;

	@NotBlank
	@Size(max = 2000)
	@ApiModelProperty(required = true, allowableValues = "range[1,2000]", example = "Samuel L. Ipsum content generator http://slipsum.com/", position = 2)
	private String content;

	@JsonCreator
	public PostPayload(@JsonProperty("author") Long author,
					   @JsonProperty("title") String title,
					   @JsonProperty("content") String content) {
		this.author = author;
		this.title = title;
		this.content = content;
	}

	public Long getAuthor() {
		return author;
	}

	public String getTitle() {
		return title;
	}

	public String getContent() {
		return content;
	}
}
