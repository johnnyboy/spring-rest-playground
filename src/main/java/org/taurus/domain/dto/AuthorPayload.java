package org.taurus.domain.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.Tag;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@ApiModel(description = "Used to create and update authors")
public class AuthorPayload {

	@NotBlank
	@ApiModelProperty(required = true, example = "Bob")
	private String firstName;

	@NotBlank
	@ApiModelProperty(required = true, example = "Bobson")
	private String lastName;

	@Email
	@NotNull
	@ApiModelProperty(required = true, example = "bobson@hotmail.com")
	private String email;

	@JsonCreator
	public AuthorPayload(@JsonProperty("firstName") String firstName,
						 @JsonProperty("lastName") String lastName,
						 @JsonProperty("email") String email) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getEmail() {
		return email;
	}
}
