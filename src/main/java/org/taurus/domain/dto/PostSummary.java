package org.taurus.domain.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.taurus.domain.dto.paging.PagedContent;

import java.time.LocalDateTime;

@ApiModel(description = "Read-only post view")
public class PostSummary implements PagedContent {

	@ApiModelProperty(readOnly = true, example = "15")
	private final Long id;

	@ApiModelProperty(readOnly = true, example = "Procrastination 101", position = 1)
	private final String title;

	@ApiModelProperty(readOnly = true, example = "Samuel L. Ipsum content generator http://slipsum.com/", position = 2)
	private final String content;

	@ApiModelProperty(readOnly = true, example = "15-05-2018 20:43:11", position = 3)
	private final LocalDateTime timestamp;

	@ApiModelProperty(readOnly = true, example = "3", position = 4)
	private final Long author;

	@JsonCreator
	public PostSummary(@JsonProperty("id") Long id,
					   @JsonProperty("title") String title,
					   @JsonProperty("content") String content,
					   @JsonProperty("timestamp") LocalDateTime timestamp,
					   @JsonProperty("author") Long author) {
		this.id = id;
		this.title = title;
		this.content = content;
		this.timestamp = timestamp;
		this.author = author;
	}

	public Long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public String getContent() {
		return content;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public Long getAuthor() {
		return author;
	}
}
