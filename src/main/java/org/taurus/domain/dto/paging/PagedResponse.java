package org.taurus.domain.dto.paging;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.domain.Page;

import java.util.Collections;
import java.util.List;

@ApiModel(description = "Type-safe paging abstraction to expose pagination-related metadata along with the actual content")
public class PagedResponse<T extends PagedContent> {

	@ApiModelProperty(readOnly = true, example = "true", notes = "true if there is any content, false otherwise")
	private final boolean hasContent;

	@ApiModelProperty(readOnly = true, example = "false", notes = "true if there is a next page, false otherwise", position = 1)
	private final boolean hasNext;

	@ApiModelProperty(readOnly = true, example = "false", notes = "true if there is a previous page, false otherwise", position = 2)
	private final boolean hasPrevious;

	@ApiModelProperty(readOnly = true, example = "1", notes = "total number of pages", position = 3)
	private final int totalPages;

	@ApiModelProperty(readOnly = true, example = "1", notes = "total number of elements on all pages", position = 4)
	private final long totalElements;

	@ApiModelProperty(readOnly = true, example = "10", notes = "maximum number of elements per page", position = 5)
	private final int pageSize;

	@ApiModelProperty(readOnly = true, example = "0", notes = "current page number", position = 6)
	private final int pageNumber;

	@ApiModelProperty(readOnly = true, example = "1", notes = "number of elements on current page", position = 7)
	private final int numberOfElements;

	@ApiModelProperty(readOnly = true, notes = "elements", position = 8)
	private final List<T> content;

	public PagedResponse(Page<T> page) {
		hasContent = page.hasContent();
		hasNext = page.hasNext();
		hasPrevious = page.hasPrevious();
		totalPages = page.getTotalPages();
		totalElements = page.getTotalElements();
		pageSize = page.getSize();
		pageNumber = page.getNumber();
		numberOfElements = page.getNumberOfElements();
		content = page.getContent();
	}

	@JsonCreator
	public PagedResponse(@JsonProperty("hasContent") boolean hasContent,
						 @JsonProperty("hasNext") boolean hasNext,
						 @JsonProperty("hasPrevious") boolean hasPrevious,
						 @JsonProperty("totalPages") int totalPages,
						 @JsonProperty("totalElements") long totalElements,
						 @JsonProperty("pageSize") int pageSize,
						 @JsonProperty("currentPage") int currentPage,
						 @JsonProperty("numberOfElements") int numberOfElements,
						 @JsonProperty("content") List<T> content) {
		this.hasContent = hasContent;
		this.hasNext = hasNext;
		this.hasPrevious = hasPrevious;
		this.totalPages = totalPages;
		this.totalElements = totalElements;
		this.pageSize = pageSize;
		this.pageNumber = currentPage;
		this.numberOfElements = numberOfElements;
		this.content = content;
	}

	@JsonProperty
	public boolean hasContent() {
		return hasContent;
	}

	@JsonProperty
	public boolean hasNext() {
		return hasNext;
	}

	@JsonProperty
	public boolean hasPrevious() {
		return hasPrevious;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public long getTotalElements() {
		return totalElements;
	}

	public int getPageSize() {
		return pageSize;
	}

	public int getCurrentPage() {
		return pageNumber;
	}

	public int getNumberOfElements() {
		return numberOfElements;
	}

	public List<T> getContent() {
		return content;
	}

	public static <T extends PagedContent> PagedResponse<T> empty() {
		return new PagedResponse<>(
				false,
				false,
				false,
				0,
				0,
				0,
				0,
				0,
				Collections.emptyList());
	}
}
