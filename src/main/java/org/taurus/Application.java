package org.taurus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.taurus.config.PersistenceConfiguration;
import org.taurus.config.RestConfiguration;
import org.taurus.config.SwaggerConfiguration;

@SpringBootApplication
@Import({PersistenceConfiguration.class, RestConfiguration.class, SwaggerConfiguration.class})
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
