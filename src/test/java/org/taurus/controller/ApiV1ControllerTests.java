package org.taurus.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockMvcClientHttpRequestFactory;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import org.taurus.domain.Author;
import org.taurus.domain.Post;
import org.taurus.domain.dto.AuthorPayload;
import org.taurus.domain.dto.AuthorSummary;
import org.taurus.domain.dto.PostPayload;
import org.taurus.domain.dto.PostSummary;
import org.taurus.domain.dto.paging.PagedContent;
import org.taurus.domain.dto.paging.PagedResponse;
import org.taurus.exception.ApiError;
import org.taurus.exception.ApiFieldValidationError;
import org.taurus.exception.ApiObjectValidationError;
import org.taurus.service.AuthorService;
import org.taurus.service.PostService;

import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@Transactional
public class ApiV1ControllerTests {

	@Autowired
	private AuthorService authorService;

	@Autowired
	private PostService postService;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private MockMvc mockMvc;

	@Before
	public void setUp() {
		restTemplate.setRequestFactory(new MockMvcClientHttpRequestFactory(mockMvc));
	}

	@Test
	public void findAllAuthorsReturnsEmptyPagedResponseOnCleanDatabase() {
		//given
		URI uri = UriComponentsBuilder.fromPath("/api/v1/authors").build().toUri();

		//when
		PagedResponse<AuthorSummary> summaries = restTemplate.exchange(uri, HttpMethod.GET, RequestEntity.EMPTY, pagedAuthorSummaryTypeReference()).getBody();

		//then
		assertEmptyPagedResponse(summaries);
	}

	@Test
	public void findAllAuthorsReturnsAllPersistedAuthors() {
		//given
		Map<Author, Set<Post>> authorsAndPosts = persistSomeAuthorsAndPosts();
		Set<Author> authors = authorsAndPosts.keySet();
		URI uri = UriComponentsBuilder.fromPath("/api/v1/authors").build().toUri();

		//when
		PagedResponse<AuthorSummary> summaries = restTemplate.exchange(uri, HttpMethod.GET, RequestEntity.EMPTY, pagedAuthorSummaryTypeReference()).getBody();

		//then
		assertThat(summaries.getNumberOfElements()).isEqualTo(authorsAndPosts.size());
		authors.forEach(author -> assertAuthorEqualToSummary(author, authorSummaryById(summaries, author.getId())));
	}

	private AuthorSummary authorSummaryById(PagedResponse<AuthorSummary> summaries, Long id) {
		return summaries
				.getContent()
				.stream()
				.filter(s -> s.getId().equals(id))
				.findFirst()
				.orElseThrow(IllegalStateException::new);
	}

	@Test
	public void findAllAuthorsSupportsPaging() {
		//given
		Map<Author, Set<Post>> authorsAndPosts = persistSomeAuthorsAndPosts();
		int maxElements = 1;
		URI uri = UriComponentsBuilder.fromPath("/api/v1/authors").queryParam("pageSize", maxElements).build().toUri();

		//when
		PagedResponse<AuthorSummary> summaries = restTemplate.exchange(uri, HttpMethod.GET, RequestEntity.EMPTY, pagedAuthorSummaryTypeReference()).getBody();

		assertThat(summaries.hasContent());
		assertThat(summaries.getNumberOfElements()).isEqualTo(maxElements);
		assertThat(summaries.getPageSize()).isEqualTo(maxElements);
		assertThat(summaries.getTotalElements()).isEqualTo(authorsAndPosts.size());
		assertThat(summaries.hasNext()).isTrue();
		assertThat(summaries.hasPrevious()).isFalse();
		assertThat(summaries.getCurrentPage()).isEqualTo(0);
		assertThat(summaries.getTotalPages()).isEqualTo(authorsAndPosts.size() / maxElements);
		assertThat(summaries.getContent().size()).isEqualTo(maxElements);
	}

	@Test
	public void findAuthorReturnsMatchingRecord() {
		//given
		Map<Author, Set<Post>> authorsAndPosts = persistSomeAuthorsAndPosts();
		Author author = authorsAndPosts.keySet().iterator().next();
		URI uri = UriComponentsBuilder.fromPath("/api/v1/authors/{id}").buildAndExpand(author.getId()).toUri();

		//when
		AuthorSummary summary = restTemplate.getForObject(uri, AuthorSummary.class);

		//then
		assertAuthorEqualToSummary(author, summary);
	}

	@Test
	public void findAuthorReturnsApiErrorOnNoMatchingRecord() {
		//given
		Integer nonExistingAuthorId = 387;
		URI uri = UriComponentsBuilder.fromPath("/api/v1/authors/{id}").buildAndExpand(nonExistingAuthorId).toUri();

		//when
		ResponseEntity<ApiError> response = restTemplate.getForEntity(uri, ApiError.class);

		//then
		assertThat(response).isNotNull();
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(response.hasBody()).isTrue();
		assertThat(response.getBody().getStatusCode()).isNotBlank();
		assertThat(response.getBody().getMessage()).contains("No Author found for id", String.valueOf(nonExistingAuthorId));
	}

	@Test
	public void createAuthorReturnsLocationHeader() {
		//given
		AuthorPayload payload = new AuthorPayload("Rob", "Reiner", "reiner@gmail.com");
		URI uri = UriComponentsBuilder.fromPath("/api/v1/authors/").build().toUri();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<AuthorPayload> body = new HttpEntity<>(payload, headers);

		//when
		URI authorUri = restTemplate.postForLocation(uri, body);
		AuthorSummary summary = restTemplate.getForObject(authorUri, AuthorSummary.class);

		//then
		assertAuthorPayloadEqualToSummary(payload, summary);
	}

	@Test
	public void createAuthorRejectsMalformedPayloadWithApiObjectValidationError() {
		//given
		AuthorPayload payload = new AuthorPayload(" ", null, "reiner.gmail.com");
		URI uri = UriComponentsBuilder.fromPath("/api/v1/authors/").build().toUri();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<AuthorPayload> body = new HttpEntity<>(payload, headers);

		//when
		ResponseEntity<ApiObjectValidationError> response = restTemplate.postForEntity(uri, body, ApiObjectValidationError.class);

		//then
		assertThat(response.hasBody());
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
		Set<ApiFieldValidationError> fieldErrors = response.getBody().getFieldErrors();
		assertThat(fieldErrors).isNotEmpty();
		assertThat(fieldErrors).hasSize(3);
		assertApiFieldValidationError(new ApiFieldValidationError("firstName", payload.getFirstName(), "must not be blank"), fieldErrors);
		assertApiFieldValidationError(new ApiFieldValidationError("lastName", payload.getLastName(), "must not be blank"), fieldErrors);
		assertApiFieldValidationError(new ApiFieldValidationError("email", payload.getEmail(), "must be a well-formed email address"), fieldErrors);
	}

	private void assertApiFieldValidationError(ApiFieldValidationError expected, Set<ApiFieldValidationError> actual) {
		assertThat(actual).contains(expected);
	}

	@Test
	public void updateAuthorUpdatesMatchingRecordReturnsNoContentAndNoBody() {
		//given
		Map<Author, Set<Post>> authorsAndPosts = persistSomeAuthorsAndPosts();
		Author author = authorsAndPosts.keySet().iterator().next();
		AuthorPayload payload = new AuthorPayload("Cob", "Stevenson", "cob@hotmail.com");
		URI uri = UriComponentsBuilder.fromPath("/api/v1/authors/{id}").buildAndExpand(author.getId()).toUri();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<AuthorPayload> body = new HttpEntity<>(payload, headers);

		//when
		ResponseEntity<?> response = restTemplate.exchange(uri, HttpMethod.PUT, body, ResponseEntity.class);
		AuthorSummary summary = restTemplate.getForObject(uri, AuthorSummary.class);

		//then
		assertThat(response.hasBody()).isFalse();
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
		assertAuthorPayloadEqualToSummary(payload, summary);

	}

	@Test
	public void deleteAuthorDeletesMatchingRecordReturnsNoContentAndNoBody() {
		//given
		Map<Author, Set<Post>> authorsAndPosts = persistSomeAuthorsAndPosts();
		Author authorToDelete = authorsAndPosts.keySet().iterator().next();
		URI uri = UriComponentsBuilder.fromPath("/api/v1/authors/{id}").buildAndExpand(authorToDelete.getId()).toUri();
		URI allSummariesUri = UriComponentsBuilder.fromPath("/api/v1/authors").build().toUri();

		//when
		ResponseEntity<?> response = restTemplate.exchange(uri, HttpMethod.DELETE, RequestEntity.EMPTY, ResponseEntity.class);
		PagedResponse<AuthorSummary> summaries = restTemplate.exchange(allSummariesUri, HttpMethod.GET, RequestEntity.EMPTY, pagedAuthorSummaryTypeReference()).getBody();

		//then
		assertThat(response.hasBody()).isFalse();
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
		boolean noSummaryFoundByDeletedAuthorId = summaries.getContent().stream().map(AuthorSummary::getId).noneMatch(id -> id.equals(authorToDelete.getId()));
		assertThat(noSummaryFoundByDeletedAuthorId);
	}

	@Test
	public void authorPostsReturnsMatchingRecords() {
		//given
		Map<Author, Set<Post>> authorsAndPosts = persistSomeAuthorsAndPosts();
		Author author = authorsAndPosts.keySet().iterator().next();
		Collection<Post> posts = authorsAndPosts.get(author);
		URI uri = UriComponentsBuilder.fromPath("/api/v1/authors/{id}/posts").buildAndExpand(author.getId()).toUri();

		//when
		PagedResponse<PostSummary> summaries = restTemplate.exchange(uri, HttpMethod.GET, RequestEntity.EMPTY, pagedPostSummaryTypeReference()).getBody();

		//then
		assertThat(summaries).isNotNull();
		assertThat(summaries.getNumberOfElements()).isEqualTo(posts.size());
		posts.forEach(post -> assertPostEqualToSummary(post, postSummaryById(summaries, post.getId())));
	}

	@Test
	public void addAuthorPostReturnsLocationHeader() {
		//given
		Map<Author, Set<Post>> authorsAndPosts = persistSomeAuthorsAndPosts();
		Author author = authorsAndPosts.keySet().iterator().next();
		PostPayload payload = new PostPayload(author.getId(), "REST-API post", "Submitted via REST API");
		URI uri = UriComponentsBuilder.fromPath("/api/v1/authors/{id}/posts").buildAndExpand(author.getId()).toUri();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<PostPayload> body = new HttpEntity<>(payload, headers);

		//when
		URI postUri = restTemplate.postForLocation(uri, body);
		PostSummary summary = restTemplate.getForObject(postUri, PostSummary.class);

		//then
		assertPostPayloadEqualToSummary(payload, summary);
	}

	@Test
	public void deleteAuthorPostReturnsNoContent() {
		//given
		Map<Author, Set<Post>> authorsAndPosts = persistSomeAuthorsAndPosts();
		Author author = authorsAndPosts.keySet().iterator().next();
		Set<Post> posts = authorsAndPosts.get(author);
		Post postToDelete = posts.iterator().next();
		Map<String, Object> uriVariables = new HashMap<>();
		uriVariables.put("authorId", author.getId());
		uriVariables.put("postId", postToDelete.getId());
		URI uri = UriComponentsBuilder.fromPath("/api/v1/authors/{authorId}/posts/{postId}").buildAndExpand(uriVariables).toUri();
		URI postsUri = UriComponentsBuilder.fromPath("/api/v1/authors/{id}/posts").buildAndExpand(author.getId()).toUri();

		//when
		restTemplate.delete(uri);
		PagedResponse<PostSummary> summaries = restTemplate.exchange(postsUri, HttpMethod.GET, RequestEntity.EMPTY, pagedPostSummaryTypeReference()).getBody();

		//then
		assertThat(summaries.getContent().stream().map(PostSummary::getId).noneMatch(id -> id.equals(postToDelete.getId())));
	}

	@Test
	public void updatePostUpdatesMatchingRecordReturnsNoContentAndNoBody() {
		//given
		Map<Author, Set<Post>> authorsAndPosts = persistSomeAuthorsAndPosts();
		Author author = authorsAndPosts.keySet().iterator().next();
		Post postToUpdate = authorsAndPosts.get(author).iterator().next();
		PostPayload payload = new PostPayload(author.getId(), "REST API edit", "Updated via REST API");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<PostPayload> body = new HttpEntity<>(payload, headers);
		Map<String, Object> uriVariables = new HashMap<>();
		uriVariables.put("authorId", author.getId());
		uriVariables.put("postId", postToUpdate.getId());
		URI uri = UriComponentsBuilder.fromPath("/api/v1/authors/{authorId}/posts/{postId}").buildAndExpand(uriVariables).toUri();

		//when
		ResponseEntity<?> response = restTemplate.exchange(uri, HttpMethod.PUT, body, ResponseEntity.class);
		PostSummary summary = restTemplate.getForObject(uri, PostSummary.class);

		//then
		assertThat(response.hasBody()).isFalse();
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
		assertPostPayloadEqualToSummary(payload, summary);
	}

	@Test
	public void updatePostRejectsMalformedPayloadWithApiObjectValidationError() {
		//given
		Map<Author, Set<Post>> authorsAndPosts = persistSomeAuthorsAndPosts();
		Author author = authorsAndPosts.keySet().iterator().next();
		Post postToUpdate = authorsAndPosts.get(author).iterator().next();
		PostPayload payload = new PostPayload(-3L, "A title exceeding size validation constraint, e.g. thirty characters limit", "Valid content");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<PostPayload> body = new HttpEntity<>(payload, headers);
		Map<String, Object> uriVariables = new HashMap<>();
		uriVariables.put("authorId", author.getId());
		uriVariables.put("postId", postToUpdate.getId());
		URI uri = UriComponentsBuilder.fromPath("/api/v1/authors/{authorId}/posts/{postId}").buildAndExpand(uriVariables).toUri();

		//when
		ResponseEntity<ApiObjectValidationError> response = restTemplate.exchange(uri, HttpMethod.PUT, body, ApiObjectValidationError.class);

		//then
		assertThat(response.hasBody());
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
		Set<ApiFieldValidationError> fieldErrors = response.getBody().getFieldErrors();
		assertThat(fieldErrors).hasSize(2);
		//FIXME long gets serialized as Integer, hence failing equals
		assertApiFieldValidationError(new ApiFieldValidationError("author", payload.getAuthor(), "must be greater than 0"), fieldErrors);
		assertApiFieldValidationError(new ApiFieldValidationError("title", payload.getTitle(), "size must be between 0 and 30"), fieldErrors);
	}

	@Test
	public void searchSupportsFilteringByAuthorTitleAndContent() {
		//given
		Author bob = persistSomeAuthorsAndPosts().keySet().stream().filter(author -> author.getFirstName().equals("Bob")).findFirst().orElseThrow(IllegalStateException::new);
		String titlePattern = "rest";
		String contentPattern = "rest";
		URI uri = UriComponentsBuilder.fromPath("/api/v1/search")
				.queryParam("author", bob.getId())
				.queryParam("title", titlePattern)
				.queryParam("content", contentPattern)
				.build().toUri();

		//when
		ResponseEntity<PagedResponse<PostSummary>> response = restTemplate.exchange(uri, HttpMethod.GET, RequestEntity.EMPTY, pagedPostSummaryTypeReference());

		//then
		assertThat(response.hasBody());
		assertThat(response.getBody().getContent()).hasSize(1);
		PostSummary post = response.getBody().getContent().iterator().next();
		assertThat(post.getAuthor()).isEqualTo(bob.getId());
		assertThat(post.getTitle()).containsIgnoringCase(titlePattern);
		assertThat(post.getContent()).containsIgnoringCase(contentPattern);
	}

	@Test
	public void getEndpointsSetEtagAndContentLengthHeaders() {
		//given
		persistSomeAuthorsAndPosts();
		URI uri = UriComponentsBuilder.fromPath("/api/v1/authors").build().toUri();

		//when
//		restTemplate.headForHeaders(uri); would not work due to default behaviour of ShallowEtagHeaderFilter org.springframework.web.filter.ShallowEtagHeaderFilter.isEligibleForEtag()
		ResponseEntity<PagedResponse<AuthorSummary>> response = restTemplate.exchange(uri, HttpMethod.GET, RequestEntity.EMPTY, pagedAuthorSummaryTypeReference());


		//then
		assertThat(response.getHeaders().getETag()).isNotBlank();
		assertThat(response.getHeaders().getContentLength()).isPositive();
	}

	@Test
	public void getEndpointsSupportEtagBasedConditionalRequests() {
		//given
		Author author = persistSomeAuthorsAndPosts().keySet().iterator().next();
		URI uri = UriComponentsBuilder.fromPath("/api/v1/authors/{id}").buildAndExpand(author.getId()).toUri();

		//when
		ResponseEntity<AuthorSummary> response = restTemplate.getForEntity(uri, AuthorSummary.class);

		//then
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(response.hasBody());

		//given
		String eTag = response.getHeaders().getETag();
		HttpHeaders headers = new HttpHeaders();
		headers.setIfNoneMatch(eTag);
		HttpEntity<?> requestEntity = new HttpEntity<>(headers);

		//when
		ResponseEntity<AuthorSummary> conditionalResponse = restTemplate.exchange(uri, HttpMethod.GET, requestEntity, AuthorSummary.class);

		//then
		assertThat(conditionalResponse.getStatusCode()).isEqualTo(HttpStatus.NOT_MODIFIED);
		assertThat(conditionalResponse.hasBody()).isFalse();

		//given
		AuthorPayload payload = new AuthorPayload("Rod", author.getLastName(), author.getEmail());
		HttpHeaders putHeaders = new HttpHeaders();
		putHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<AuthorPayload> body = new HttpEntity<>(payload, putHeaders);

		//when
		ResponseEntity<ResponseEntity> putResponse = restTemplate.exchange(uri, HttpMethod.PUT, body, ResponseEntity.class);

		//then
		assertThat(putResponse.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);

		//given
		HttpHeaders postUpdateHeaders = new HttpHeaders();
		postUpdateHeaders.setIfNoneMatch(eTag);
		HttpEntity<?> postUpdateRequestEntity = new HttpEntity<>(postUpdateHeaders);

		//when
		ResponseEntity<AuthorSummary> postUpdateResponse = restTemplate.exchange(uri, HttpMethod.GET, postUpdateRequestEntity, AuthorSummary.class);

		//then
		assertThat(postUpdateResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(postUpdateResponse.hasBody());
		assertThat(postUpdateResponse.getHeaders().getETag()).isNotEqualTo(eTag);
	}

	private PostSummary postSummaryById(PagedResponse<PostSummary> summaries, Long id) {
		return summaries
				.getContent()
				.stream()
				.filter(s -> s.getId().equals(id))
				.findFirst()
				.orElseThrow(IllegalStateException::new);
	}

	private ParameterizedTypeReference<PagedResponse<AuthorSummary>> pagedAuthorSummaryTypeReference() {
		return new ParameterizedTypeReference<PagedResponse<AuthorSummary>>() {
		};
	}

	private ParameterizedTypeReference<PagedResponse<PostSummary>> pagedPostSummaryTypeReference() {
		return new ParameterizedTypeReference<PagedResponse<PostSummary>>() {
		};
	}

	private <T extends PagedContent> void assertEmptyPagedResponse(PagedResponse<T> response) {
		assertThat(response).isNotNull();
		assertThat(response.hasContent()).isFalse();
		assertThat(response.getCurrentPage()).isEqualTo(0);
		assertThat(response.getPageSize()).isEqualTo(10);
		assertThat(response.getNumberOfElements()).isEqualTo(0);
		assertThat(response.getTotalElements()).isEqualTo(0);
		assertThat(response.getContent()).isEmpty();
	}

	private void assertAuthorEqualToSummary(Author expected, AuthorSummary actual) {
		assertThat(expected).isNotNull();
		assertThat(actual).isNotNull();
		assertThat(actual.getId()).isEqualTo(expected.getId());
		assertThat(actual.getFirstName()).isEqualTo(expected.getFirstName());
		assertThat(actual.getLastName()).isEqualTo(expected.getLastName());
		assertThat(actual.getEmail()).isEqualTo(expected.getEmail());
		assertThat(actual.getPostsCount()).isEqualTo(expected.getPosts().size());
	}

	private void assertAuthorPayloadEqualToSummary(AuthorPayload expected, AuthorSummary actual) {
		assertThat(expected).isNotNull();
		assertThat(actual).isNotNull();
		assertThat(actual.getFirstName()).isEqualTo(expected.getFirstName());
		assertThat(actual.getLastName()).isEqualTo(expected.getLastName());
		assertThat(actual.getEmail()).isEqualTo(expected.getEmail());
	}

	private void assertPostEqualToSummary(Post expected, PostSummary actual) {
		assertThat(expected).isNotNull();
		assertThat(actual).isNotNull();
		assertThat(actual.getId()).isEqualTo(expected.getId());
		assertThat(actual.getAuthor()).isEqualTo(expected.getAuthor().getId());
		assertThat(actual.getTitle()).isEqualTo(expected.getTitle());
		assertThat(actual.getContent()).isEqualTo(expected.getContent());
		assertThat(actual.getTimestamp()).isEqualTo(expected.getTimestamp());
	}

	private void assertPostPayloadEqualToSummary(PostPayload expected, PostSummary actual) {
		assertThat(expected).isNotNull();
		assertThat(actual).isNotNull();
		assertThat(actual.getAuthor()).isEqualTo(expected.getAuthor());
		assertThat(actual.getTitle()).isEqualTo(expected.getTitle());
		assertThat(actual.getContent()).isEqualTo(expected.getContent());
	}

	private Map<Author, Set<Post>> persistSomeAuthorsAndPosts() {
		Map<Author, Set<Post>> map = new HashMap<>();

		Author bob = authorService.save("Bob", "Bobson", "bob@hotmail.com");
		Post howToRest = postService.add(bob, "Brief guide to REST API with Spring Boot", "How-to REST");
		Post summerFestivalsRecap = postService.add(bob, "Places, bands & memories", "Summer festivals recap");
		map.put(bob, new HashSet<>(Arrays.asList(howToRest, summerFestivalsRecap)));

		Author jane = authorService.save("Jane", "Doe", "jane@yahoo.com");
		Post barcelona = postService.add(jane, "Air-fares, housing & sightseeing", "Barcelona - my new favourite city");
		map.put(jane, Collections.singleton(barcelona));

		Author angelina = authorService.save("Angelina", "Dickinson", "angelina@bing.com");
		map.put(angelina, Collections.emptySet());
		return map;
	}
}
