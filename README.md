### spring-rest-playground  
[![Run Status](https://api.shippable.com/projects/5b5f085c36c8e80700df2556/badge?branch=master)](https://app.shippable.com/bitbucket/johnnyboy/spring-rest-playground)
[![Coverage Badge](https://api.shippable.com/projects/5b5f085c36c8e80700df2556/coverageBadge?branch=master)](https://app.shippable.com/bitbucket/johnnyboy/spring-rest-playground)  

Spring Boot - powered REST API, showcasing following concepts

* API layer operates solely on DTOs, no domain exposure  
* Hierarchical resources and collections
* Idiomatic CRUD via HTTP methods
* Paging support  
* Simple search endpoint  
* Conditional requests via ETag support  
* Payload JSR-380 validation (Hibernate validator)  
* User-friendly errors  
* RestTemplate-based integration tests  
* API documentation generation  

#### Libraries used:

* spring-boot-starter-web
* spring-boot-starter-data-jpa  
* API documentation generation via [Swagger](https://swagger.io/)  
* Code coverage via [JaCoCo](https://www.eclemma.org/jacoco/)
* CI support by [Shippable](http://www.shippable.com/)

#### Build and generate coverage report
`mvn clean package`

#### Run
`mvn spring-boot:run`  

`http://localhost:8080/api/v1` - API base URL  
`http://localhost:8080/swagger-ui.html` - API documentation